﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace Zeiterfassung
{
    public partial class Form1 : Form
    {
        private static readonly Stopwatch stopWatch = new Stopwatch();
        public Form1()
        {
            InitializeComponent();
            UpdateTime();
            UpdateDate();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "Start")
            {
                stopWatch.Start();
                btnStart.Text = "Pause";
                timer.Enabled = true; 
            }
            else
            {
                stopWatch.Stop();
                btnStart.Text = "Start";
                timer.Enabled = false;
            }            
        }

        private void UpdateTime()
        {
            tbxZeit.Text = GetTimeString(stopWatch.Elapsed);
        }

        private string GetTimeString(TimeSpan elapsed)
        {
            string result = string.Empty;
            result = string.Format("{0:00}:{1:00}:{2:00}",
                elapsed.Hours,
                elapsed.Minutes,
                elapsed.Seconds);

            return result;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            stopWatch.Stop();
            timer.Enabled = false;
            btnStart.Text = "Start";
            DialogResult result = MessageBox.Show("Bearbeitung beenden und in Datei speichern?", "Bearbeitung speichern?", MessageBoxButtons.YesNo);
            if(result == DialogResult.Yes)
            {
                string timeString = GetTimeString(stopWatch.Elapsed).ToString();
                ExportCsv(timeString);
            }
        }

        private void ExportCsv(string timeString)
        {
            StringBuilder CsvContent = new StringBuilder();
            string daten = lblDatum.Text.ToString() + ";" + tbxKunde.Text.ToString() + ";" + timeString + ";" + rtbNotiz.Text.ToString();
            CsvContent.AppendLine(daten);

            string csvPath = @"C:\Users\Dirk\OneDrive\Weiterbildung1\Fachinformatiker\Programme\WinForm\Zeiterfassung\Test1.csv";
            File.AppendAllText(csvPath, CsvContent.ToString());
            MessageBox.Show("Daten in CSV gesichert");
        }

        private void tbxZeit_TextChanged(object sender, EventArgs e)
        {
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateTime();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {

            if(timer.Enabled == false)
            {
                DialogResult result = MessageBox.Show("Stopuhr zurücksetzen", "Zurücksetzen?", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    stopWatch.Reset();
                    UpdateTime();
                }
            }
        }

        private void UpdateDate()
        {
            DateTimePicker heute = new DateTimePicker();
            lblDatum.Text = GetDateString(heute.Value);
        }

        private string GetDateString(DateTime value)
        {
            string result = string.Empty;
            result = string.Format("{0}:{1}:{2}:{3}",
                
                value.DayOfWeek,
                value.Day,
                value.Month,
                value.Year);

            return result;
        }
    }
}
