﻿namespace Zeiterfassung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.tbxKunde = new System.Windows.Forms.TextBox();
            this.cbxKunde = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.rtbNotiz = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDbUpload = new System.Windows.Forms.Button();
            this.btnCsvExport = new System.Windows.Forms.Button();
            this.btnDbLaden = new System.Windows.Forms.Button();
            this.tbxZeit = new System.Windows.Forms.TextBox();
            this.lblZeit = new System.Windows.Forms.Label();
            this.btnCsvImport = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btnReset = new System.Windows.Forms.Button();
            this.lblDatum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(36, 25);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(120, 77);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tbxKunde
            // 
            this.tbxKunde.Location = new System.Drawing.Point(33, 140);
            this.tbxKunde.Name = "tbxKunde";
            this.tbxKunde.Size = new System.Drawing.Size(203, 20);
            this.tbxKunde.TabIndex = 2;
            // 
            // cbxKunde
            // 
            this.cbxKunde.FormattingEnabled = true;
            this.cbxKunde.Location = new System.Drawing.Point(36, 204);
            this.cbxKunde.Name = "cbxKunde";
            this.cbxKunde.Size = new System.Drawing.Size(200, 21);
            this.cbxKunde.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kunde eingeben";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Kunde auswählen";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(176, 25);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(120, 77);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Bearbeitung Ende";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // rtbNotiz
            // 
            this.rtbNotiz.Location = new System.Drawing.Point(36, 268);
            this.rtbNotiz.Name = "rtbNotiz";
            this.rtbNotiz.Size = new System.Drawing.Size(404, 156);
            this.rtbNotiz.TabIndex = 8;
            this.rtbNotiz.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Notizen";
            // 
            // btnDbUpload
            // 
            this.btnDbUpload.Location = new System.Drawing.Point(668, 321);
            this.btnDbUpload.Name = "btnDbUpload";
            this.btnDbUpload.Size = new System.Drawing.Size(120, 43);
            this.btnDbUpload.TabIndex = 10;
            this.btnDbUpload.Text = "Upload - Datenbank";
            this.btnDbUpload.UseVisualStyleBackColor = true;
            // 
            // btnCsvExport
            // 
            this.btnCsvExport.Location = new System.Drawing.Point(668, 385);
            this.btnCsvExport.Name = "btnCsvExport";
            this.btnCsvExport.Size = new System.Drawing.Size(120, 39);
            this.btnCsvExport.TabIndex = 11;
            this.btnCsvExport.Text = "CSV Export";
            this.btnCsvExport.UseVisualStyleBackColor = true;
            // 
            // btnDbLaden
            // 
            this.btnDbLaden.Location = new System.Drawing.Point(529, 321);
            this.btnDbLaden.Name = "btnDbLaden";
            this.btnDbLaden.Size = new System.Drawing.Size(120, 39);
            this.btnDbLaden.TabIndex = 12;
            this.btnDbLaden.Text = "Download - Datenbank";
            this.btnDbLaden.UseVisualStyleBackColor = true;
            // 
            // tbxZeit
            // 
            this.tbxZeit.Location = new System.Drawing.Point(458, 82);
            this.tbxZeit.Name = "tbxZeit";
            this.tbxZeit.Size = new System.Drawing.Size(100, 20);
            this.tbxZeit.TabIndex = 13;
            this.tbxZeit.TextChanged += new System.EventHandler(this.tbxZeit_TextChanged);
            // 
            // lblZeit
            // 
            this.lblZeit.AutoSize = true;
            this.lblZeit.Location = new System.Drawing.Point(458, 63);
            this.lblZeit.Name = "lblZeit";
            this.lblZeit.Size = new System.Drawing.Size(25, 13);
            this.lblZeit.TabIndex = 14;
            this.lblZeit.Text = "Zeit";
            // 
            // btnCsvImport
            // 
            this.btnCsvImport.Location = new System.Drawing.Point(529, 385);
            this.btnCsvImport.Name = "btnCsvImport";
            this.btnCsvImport.Size = new System.Drawing.Size(120, 39);
            this.btnCsvImport.TabIndex = 15;
            this.btnCsvImport.Text = "CSV Import";
            this.btnCsvImport.UseVisualStyleBackColor = true;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(320, 25);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(120, 77);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Location = new System.Drawing.Point(701, 25);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(38, 13);
            this.lblDatum.TabIndex = 16;
            this.lblDatum.Text = "Datum";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.btnCsvImport);
            this.Controls.Add(this.lblZeit);
            this.Controls.Add(this.tbxZeit);
            this.Controls.Add(this.btnDbLaden);
            this.Controls.Add(this.btnCsvExport);
            this.Controls.Add(this.btnDbUpload);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rtbNotiz);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxKunde);
            this.Controls.Add(this.tbxKunde);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox tbxKunde;
        private System.Windows.Forms.ComboBox cbxKunde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.RichTextBox rtbNotiz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDbUpload;
        private System.Windows.Forms.Button btnCsvExport;
        private System.Windows.Forms.Button btnDbLaden;
        private System.Windows.Forms.TextBox tbxZeit;
        private System.Windows.Forms.Label lblZeit;
        private System.Windows.Forms.Button btnCsvImport;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblDatum;
    }
}

